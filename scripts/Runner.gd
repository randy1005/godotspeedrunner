extends KinematicBody2D

# defining constants
const GRAVITY = 20
const SPEED = 300
const JUMP_H = -500
const RESISTANCE = Vector2(0, -1)

var runnerMotion = Vector2()

func _physics_process(delta):
	runnerMotion.y += GRAVITY
	
	if Input.is_action_pressed("ui_right"):
		runnerMotion.x = SPEED
		# set horizontal flip to false
		$runner_sprite.flip_h = false
		$runner_sprite.play("Running")
		
	elif Input.is_action_pressed("ui_left"):
		runnerMotion.x = -SPEED
		# set horizontal flip to true
		$runner_sprite.flip_h = true
		$runner_sprite.play("Running")
		
	else:
		runnerMotion.x = 0
		$runner_sprite.play("Idle")
		
	if is_on_floor():
		if Input.is_action_just_pressed("ui_accept"):
			runnerMotion.y = JUMP_H
	else:
		# going up
		if runnerMotion.y < 0:
			$runner_sprite.play("Rise")
		else:
			$runner_sprite.play("Fall")
	
	runnerMotion = move_and_slide(runnerMotion, RESISTANCE)