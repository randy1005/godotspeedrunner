extends Node


func _on_hostButton_pressed():
	print("host")
	var root = get_tree().get_root().get_node("LobbyScreen")
	var player_name = root.find_node("hostName").text
	if player_name != "":
		ConnManager.on_host_game()


func _on_joinButton_pressed():
	print("join")
	var root = get_tree().get_root().get_node("LobbyScreen")
	var player_name = root.find_node("joinName").text
	var ip = root.find_node("IP_bar").text
	if player_name != "" and ip != "":
		ConnManager.on_join_game(ip)
