extends RigidBody2D

var speed = 150
var rotSpeed = 80
var maxSpeed = 100

# where the magic happens, slave means this var is to be set by others
slave var slavePos = Vector2()
slave var slaveRot = 0

func _ready():
	pass

func _process(delta):
	
	if is_network_master():
	
		if Input.is_action_pressed("ui_up"):
			add_force(Vector2(0, 0), Vector2(cos(rotation), sin(rotation)) * delta * speed)
		
		# limit max speed
		if get_linear_velocity().length() > maxSpeed:
			set_linear_velocity(get_linear_velocity().normalized() * maxSpeed)
		
		
		if Input.is_action_pressed("ui_left"):
			set_angular_velocity(-rotSpeed * delta)
	
		if Input.is_action_pressed("ui_right"):
			set_angular_velocity(rotSpeed * delta)
			
		# master informs about new rotation & position
		rset_unreliable("slavePos", position)
		rset_unreliable("slaveRot", rotation)
	
	else:
		# slave gets the received pos & rot then update
		position = slavePos
		rotation = slaveRot


